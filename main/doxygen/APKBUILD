# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=doxygen
pkgver=1.9.2
pkgrel=0
pkgdesc="A documentation system for C++, C, Java, IDL and PHP"
url="https://www.doxygen.nl/"
arch="all"
license="GPL-2.0-or-later"
checkdepends="libxml2-utils"
makedepends="flex bison coreutils perl python3 cmake"
source="https://doxygen.nl/files/doxygen-$pkgver.src.tar.gz"

[ "$CARCH" = "riscv64" ] && options="textrels"

build() {
	cmake -B build \
		-DGIT_EXECUTABLE=/bin/false \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=None \
		-Dbuild_xmlparser=ON .
	make -C build
}

check() {
	# Remove test that use bibtex
	rm -f ./testing/012_cite.dox
	make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="
2729d013b0363a919bbf6babd300389b76e95dd9dbb16ac0f57fc5a0392d3e2076ea4fba958a236311513e68e4aa056a77bd22c9c92b410a17eed095e5adccc0  doxygen-1.9.2.src.tar.gz
"
